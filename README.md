Introduction
------------
The goal of the xcorpus project is to provide an *executable* version of the [qualitas corpus](http://qualitascorpus.com/). There are several use cases for this, for instance to execute a program to verify the output of static verification tools and to assess the impact of features that are notoriously difficult to capture with static analysis, such as reflection.

Version History
---------------

1.0.0 initial release , 20 April 2017

To start using the corpus, clone the latest release: 
`hg clone https://jensdietrich@bitbucket.org/jensdietrich/xcorpus -u 1.0.0`.

Paper
-----

This paper describes the xcorpus in detail: [Jens Dietrich, Henrik Schole, Li Sui, Ewan Tempero, �XCorpus � An executable Corpus of Java Programs�, Journal of Object Technology, Volume 16, no. 4, 2017 ](http://www.jot.fm/issues/issue_2017_04/article1.pdf).

Structure
---------
The xcorpus is based on the [qualitas corpus version 20130901](http://qualitascorpus.com/). The `data` folder contains a folder `qualitas_corpus_20130901`, which contains the individual corpus projects. Each project folder has an additional subfolder `.xcorpus/` with some added files necessary to execute (*excercise*) the respective program, in particular an *ant* script `exercise.xml`.

There are additional data sets in the `data` folder to augment the [qualitas corpus](http://qualitascorpus.com/) programs. For an explanation of the selection process, please refer to our paper (URL of final version TBA, tex sources in `paper`).

SetUp
-----------
The following set up is required to use the corpus.

*  **JDK**: use 1.8.0_60 or better, use `java -version` to verify
*  **ant**: download and install [ant-1.9.7](http://archive.apache.org/dist/ant/binaries/apache-ant-1.9.7-bin.zip), and set `ANT_HOME` to point to the ANT installation root folder, use `ant -version` to verify
*  **ivy**: download and install [ivy-2.4.0](http://central.maven.org/maven2/org/apache/ivy/ivy/2.4.0/ivy-2.4.0.jar) by copying the jar into `$ANT_HOME/lib`
*  **junit**: download and install [junit-4.12](http://central.maven.org/maven2/junit/junit/4.12/junit-4.12.jar) by copying the jar into `$ANT_HOME/lib`
*  **hamcrest**: download and install [hamcrest-1.3](http://central.maven.org/maven2/org/hamcrest/hamcrest-all/1.3/hamcrest-all-1.3.jar) by copying the jar into `$ANT_HOME/lib`


Exercising XCorpus
-----------
There are two kinds of exercises for each program: (1) built-in test cases (if there are any) - note that we only use tests that succeed (2) test cases generated using [evosuite](http://www.evosuite.org/). The objective is to provide a harness with maximum *branch coverage*. Exercises are implemented as scripts with unit tests using [JUnit](http://junit.org/) and [ANT](http://ant.apache.org/). [Jacoco](http://www.eclemma.org/jacoco/) is used to measure coverage.

Execution of a specific test suite:

2. cd into the respective project folder `<project>/.xcorpus`
3. run `ant -f exercise.xml all-tests`, this runs both generated tests and built-in tests
4. wait :-) - note that the first time you run this is takes longer as the libs must be fetched from the repository, after this, libs will be cached by ivy
5. coverage metrics in different formats are written to `<project>/.xcorpus/output/TIMESTAMP`

Execution of all test suites at once:

2. cd into the xcorpus tools folder `<tools>`
3. run `ant all-tests` it will run for __approximately 31 hours__ (tested on a Ubuntu16.04, 2.3 GHz Intel Core i3 CPU with 16 GB 1600 MHz DDR3)
4. wait :-)
5. coverage metrics in different formats are written to `<project>/.xcorpus/output/TIMESTAMP`

Raw test result data will be reported in the `<project>/.xcorpus/output/<datestamp>/junit-report` folders in an XML format suitable for further processing and reporting.

Exercise the XCorpus in a Docker Container
-----------

We provide support to run XCorpus in a [docker container](https://www.docker.com/community-edition). To use this, clone the repository into a folder, cd into this folder and run the following command:

`docker build -t xcorpus-1.0.0 .`

followed by:

`docker run  -i -t xcorpus-1.0.0:latest`

This has been tested with docker community edition version 17.03.1 . 

Issues (IMPORTANT)
------

Tests are executed in forked JVM instances to ensure that a relatively small number of tests causing JVM crashes don't stop the overall script from executing. XCorpus enforces timeouts and memory limits on forked JVMs, the respective settings can be configured, for details, see below.

When a forked JVM executing a test crashes, the OS may decide to pop up a notification in a modal dialogue, and this can interrupt the execution of the script. This is best avoided by running tests on a server OS without GUI (such as Ubuntu Server). Other OSs may have a configuration option to switch those notifications off, for instance, for OSX see the discussion [here](http://stackoverflow.com/questions/20226802/disable-the-last-time-you-opened-it-unexpectedly-quit-while-reopening-window).

Some projects have sub-projects. (e.g. aspectj-1.6.9). You can checkout branch: mergeSubproject for merged version. Duplicate classes are exist in those projects. Note that the merged version simply overwrites the classes that have the same class name. In the reality, there are two cases:

1. duplicate classes are identical, then overwriting will have no impact.
2. duplicate classes are not identical. It is not deterministic as we don't know which one will be loaded. We will provide a list for each project. see issue#14


Global Ant Tasks (tools/build.xml)
----------
* `ant clean-all` : clean all temp files, generated files
* `ant generated-tests` run all tests that generated by evosuite
* `ant buildin-tests` run all tests that are provided by each project.
* `ant generate-tests` run evosuite to (re-)generate tests for all systems - this will take a very long time to run
* `run-qualitascorpus-ReportAggregator` `run-xcorpusplus-ReportAggregator` runs the aggregator to refine project report files. Specify which ones by the report attribute (`-Dreport=classtypes|evosuite|builtin-tests|generated-tests|all-tests`).

Customisation
-------------

1. to customise xcorpus, edit `tools/res/commons.properties`
2. for instance, assertion checking in generated tests can be enabled/disabled
3. tests are executed in forked JVMs - and in some cases, those JVMs might crash due to unrecoverable errors (e.g., out of memory), `tools/res/commons.properties` contains some properties to configure the behavior of forked JVMs, in particular timeouts and heap size 
4. for details, see [Configuration](https://bitbucket.org/jensdietrich/xcorpus/wiki/Configuration)

Miscellaneous
------------

1. `misc/dacapo-9.12` contains (ant) scripts to measure the coverage of [dacapo](http://dacapobench.org/), reported in the paper for comparison
2. `misc/featureanalysis` contains (ant) scripts to analyse the use of language features by programs in the dataset, and to count built-in and generated tests, the results are reported in the paper 

Known Issues
------------

Please check [the repository issue system](https://bitbucket.org/jensdietrich/xcorpus/issues?status=new&status=open) for open issues.
