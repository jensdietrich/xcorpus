FROM openjdk:8u121-jdk
RUN mkdir /xcorpus
WORKDIR /xcorpus
RUN mkdir ant
ADD ./misc/docker/ant.zip /xcorpus/ant
WORKDIR /xcorpus/ant
RUN unzip /xcorpus/ant/ant.zip
WORKDIR /xcorpus
ENV ANT_HOME /xcorpus/ant/apache-ant-1.10.1
ENV PATH ${PATH}:${ANT_HOME}/bin
ADD . /xcorpus/tmp
WORKDIR /xcorpus/tmp/tools
RUN ${ANT_HOME}/bin/ant release
WORKDIR /xcorpus
RUN unzip /xcorpus/tmp/xcorpus-1.0.0.zip