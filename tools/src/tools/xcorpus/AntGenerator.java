package xcorpus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.StringWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Properties;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;

public class AntGenerator{

        public static void main(String[] args){
                iteratorStarter(args);   
        }

        public static void iteratorStarter(String[] args){
                if(args.length>0){
                        if(args[0].equals("${projectName}"))    //no project specified
                                recursiveScriptGenerator(new File("../data/qualitas_corpus_20130901"), 0);  
                                
                        else{
                                File projects = new File("../data/qualitas_corpus_20130901/");
                                File[] list = projects.listFiles();
                                boolean projectFound=false;
                                for(int i=0;i<list.length;i++){
                                        if(list[i].getName().contains(args[0])){
                                                projectFound=true;
                                                System.out.println("found project " + list[i].getName());
                                                recursiveScriptGenerator(new File("../data/qualitas_corpus_20130901/" + list[i].getName()), 0);
                                                //generateStandardBuildScripts();
                                        }
                                }
                                if(!projectFound)
                                        System.out.println("could not find applicable project for projectName " + args[0]);
                        }        
                }
                        

                else if(args.length==0)
                        recursiveScriptGenerator(new File("../data/qualitas_corpus_20130901"), 0);    
        }

        public static void recursiveScriptGenerator(File parentDirectory, int depth){

                depth++;

                if(parentDirectory != null && depth<3){
                        System.out.println("entering " + parentDirectory.toString());
                        File[] list = parentDirectory.listFiles();
                        if(list!=null){
                                for(File child: list){
                                        if(child.isDirectory()){
                                                String childName = child.toString();
                                                if((new File(childName + "/project").exists())
                                                        && (new File(childName + "/.xcorpus").exists())             
                                                        && !(new File(childName + "/.xcorpus/exercise.xml").exists())
                                                        && !(new File(childName + "/.xcorpus/ivyFile.xml").exists())
                                                        && !(new File(childName + "/.xcorpus/evosuite-files").exists())                                   
                                                        ){
                                                        System.out.println("generate scripts for " + childName);

                                                        try {
                                                                generateProjectBuildScript(child);
                                                                generateIvyFile(child);
								generateEvosuiteProperties(child);
                                                        }catch(Exception e){
                                                                e.printStackTrace();
                                                        }
                                                }
                                                
                                                else{
                                                        recursiveScriptGenerator(child, depth);
                                                }
                                        }
                                }
                        }
                }
        }
    
        private static void generateProjectBuildScript(File projectDirectory) throws Exception{
    	
        
                VelocityEngine ve = new VelocityEngine();
                ve.init();
        
	        Template t = ve.getTemplate( "res/antTemplate.vm" );
        
        	VelocityContext context = new VelocityContext();
                context = putStandardKeyValues(projectDirectory, context);
                context = putClasspathValues(projectDirectory, context);
        
                StringWriter writer = new StringWriter();
                t.merge( context, writer );
        
                FileWriter fWriter = new FileWriter(projectDirectory.toString() + "/.xcorpus/exercise.xml");
                fWriter.append(writer.toString());
                fWriter.close();
        
        }
    
        public static VelocityContext putStandardKeyValues(File projectDirectory, VelocityContext context){
	
        context.put("projectName", projectDirectory.getName());
        context.put("projectFolder", "../project");
        context.put("xcorpusTools","../../../../tools");
	context.put("buildfolder", "build");
		
    	return context;
    }
    
    public static VelocityContext putClasspathValues(File projectDirectory, VelocityContext context) throws Exception{
    	// build JDOM document
    			SAXBuilder builder = new SAXBuilder();
    			Document doc = new Document();
				
				try{
					doc = builder.build(new FileInputStream(projectDirectory + "/project/.classpath"));					
				}catch(Exception e){
					System.out.println(".classpath file not found");
					return context;
				}
    			    			
    			String sourceFolders = "";
    			String compileSources = "";
    			String compileTestSources = "";
    			String testfiles = "";
    			
    			
    			Element root = doc.getRootElement();
    			for(Element e :root.getChildren()){
   					if(e.getAttributeValue("kind").equals("src")){
   	    				if(e.getAttribute("excluding")==null){
   	    					
   	    					String folder = e.getAttributeValue("path");
   						
   	    					String[] s = folder.split("/");
    					
    						if(folder.contains("test")){
    							sourceFolders = sourceFolders.concat("\t<property name=\"" + s[0] + ".test.dir\" location=\"${project.dir}/" + folder + "\"/>\n");
    							compileTestSources = compileTestSources.concat("${" + s[0] + ".test.dir" + "}:");
    							testfiles = testfiles.concat("\t\t\t\t<fileset dir=\"${" + s[0] + ".test.dir" + "}\"> \n\t\t\t\t\t<include name=\"**/*Test*.java\" />\n\t\t\t\t</fileset>\n");
    						}
    						else{
    							sourceFolders=sourceFolders.concat("\t<property name=\"" + s[0] + ".src.dir\" location=\"${project.dir}/" + folder + "\"/>\n");
    							compileSources = compileSources.concat("${" + s[0] + ".src.dir" + "}:");    					
    							
    						}  					
    					}
    				}
    			}
    			if(compileSources.endsWith(":"))
    				compileSources = compileSources.substring(0, compileSources.length()-1);
    			if(compileTestSources.endsWith(":"))
    				compileTestSources = compileTestSources.substring(0, compileTestSources.length()-1);    			
    			
    			context.put("sourcefolders", sourceFolders);
    			context.put("compileSources", compileSources);
    			context.put("compileTestSources", compileTestSources);
    			context.put("testFiles", testfiles);
    			
    			return context;
        }
	
	public static void generateIvyFile (File projectDirectory) throws Exception{
        
               /* initialize the engine  */
               VelocityEngine ve = new VelocityEngine();
               ve.init();
               /* get the Template  */
               Template t = ve.getTemplate( "res/ivyTemplate.vm" );
                
               /*  create a context and add data */
               VelocityContext context = new VelocityContext();
        		
        	context.put("moduleName", projectDirectory.getName());
        	context.put("organisationName", "MasseyUniversity");
        	context = putIvyDependencies(projectDirectory.getName(), context);
        	
                StringWriter writer = new StringWriter();
                t.merge( context, writer );
                
                FileWriter fWriter = new FileWriter(projectDirectory.toString() + "/.xcorpus/ivy.xml");
                fWriter.append(writer.toString());
                fWriter.close();    
        }
	
	public static VelocityContext putIvyDependencies(String projectName, VelocityContext context) throws Exception{
		
		Properties properties = new Properties();
		String dependencies = "";
		
			properties.load(new FileInputStream("res/dependencies.properties"));
			
			LinkedList<String> libs = getDependentLibraries(properties, projectName);
		
			System.out.println("found " + libs.size() + " libraries for " + projectName);
			
			for(String s : libs){
				String dependency = "\t\t<dependency org=\"";
				dependency = dependency.concat(properties.getProperty(projectName + "_" + s + "_organisation") + "\" name=\"");
				dependency = dependency.concat(properties.getProperty(projectName + "_" + s + "_name") + "\" rev=\"");
				dependency = dependency.concat(properties.getProperty(projectName + "_" + s + "_version") + "\"/>\n");
				dependencies = dependencies.concat(dependency);
			}
		
		context.put("dependencies", dependencies);
		
		return context;
	}
	
	/**
		returns list of all libraries a given project depends on (according to a given property file)
	*/
	private static LinkedList<String> getDependentLibraries(Properties properties, String projectName){
		LinkedList<String> list = new LinkedList<String>();
		
		for (Enumeration<?> e = properties.propertyNames(); e.hasMoreElements(); ) {
			String name = (String)e.nextElement();
			if(name.startsWith(projectName)){
				String[] s = name.split("_");
				
				if(s.length==3)
				if(!list.contains(s[1]))
					list.add(s[1]);
				
			}
		}
		return list;
	}

	public static void generateEvosuiteProperties(File projectDirectory) throws Exception{
        
		File file= new File (projectDirectory.toString() + "/.xcorpus/evosuite-files" );
		if(!file.exists()) file.mkdir();
			else return;

                VelocityEngine ve = new VelocityEngine();
                ve.init();
        
	        Template t = ve.getTemplate( "res/evosuiteTemplate.vm" );
        
        	VelocityContext context = new VelocityContext();
                context = putClassPath(projectDirectory.toString(), context);
        
                StringWriter writer = new StringWriter();
                t.merge( context, writer );
        
                FileWriter fWriter = new FileWriter(projectDirectory.toString() + "/.xcorpus/evosuite-files/evosuite.properties");
                fWriter.append(writer.toString());
                fWriter.close();
	}

	public static VelocityContext putClassPath(String projectDirectory, VelocityContext context) throws Exception{
			
		File defaultLibFolder = new File (projectDirectory + "/project/default-lib");

		String classPath = "build/main";

		if(!defaultLibFolder.exists()){
			System.out.println("project/default-lib folder not found, classpath in evosuite.properties maybe incomplete (might need manual editing)");
			context.put("classPath",classPath);
			return context;
		}


		String[] libs = defaultLibFolder.list();
			
		for(String s : libs){
			String lib = ":../project/default-lib/";
			lib = lib.concat(s);
			classPath = classPath.concat(lib);
		}
		
		context.put("classPath", classPath);
		
		return context;
	}
}
