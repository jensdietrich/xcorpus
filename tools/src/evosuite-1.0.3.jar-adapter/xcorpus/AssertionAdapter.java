package xcorpus;

import static org.junit.Assert.*;

public class AssertionAdapter{

        private static boolean assertsOn;

        static{
                AssertionAdapter.assertsOn = Boolean.parseBoolean(System.getProperty("xcorpus.assertsOn"));
        }

        public static void main(String[] args){
                System.out.println(System.getProperty("xcorpus.assertsOn"));
                switchAssertsOn();
                System.out.println(System.getProperty("xcorpus.assertsOn"));
        }

        public static void switchAssertsOn(){
                System.setProperty("xcorpus.assertsOn", Boolean.toString(!assertsOn));
        }


        public static void assertArrayEquals(boolean[] expecteds, boolean[] actuals){
                if(assertsOn)
                        org.junit.Assert.assertArrayEquals(expecteds, actuals);
        }

        public static void assertArrayEquals(byte[] expecteds, byte[] actuals){
                if(assertsOn)
                        org.junit.Assert.assertArrayEquals(expecteds, actuals);
        }

        public static void assertArrayEquals(char[] expecteds, char[] actuals){
                if(assertsOn)
                        org.junit.Assert.assertArrayEquals(expecteds, actuals);
        }

        public static void assertArrayEquals(double[] expecteds, double[] actuals, double delta){
                if(assertsOn)
                        org.junit.Assert.assertArrayEquals(expecteds, actuals, delta);
        }

        public static void assertArrayEquals(float[] expecteds, float[] actuals, float delta){
                if(assertsOn)
                        org.junit.Assert.assertArrayEquals(expecteds, actuals, delta);
        }

        public static void assertArrayEquals(int[] expecteds, int[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(expecteds, actuals);
        }


        public static void assertArrayEquals(long[] expecteds, long[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(expecteds, actuals);
        }

        public static void assertArrayEquals(java.lang.Object[] expecteds, java.lang.Object[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(expecteds, actuals);
        }

        public static void assertArrayEquals(short[] expecteds, short[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(expecteds, actuals);
        }

        public static void assertArrayEquals(java.lang.String message, boolean[] expecteds, boolean[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(expecteds, actuals);
        }

        public static void assertArrayEquals(java.lang.String message, byte[] expecteds, byte[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(expecteds, actuals);
        }


        public static void assertArrayEquals(java.lang.String message, char[] expecteds, char[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(message, expecteds, actuals);
        }

        public static void assertArrayEquals(java.lang.String message, double[] expecteds, double[] actuals, double delta){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(message, expecteds, actuals, delta);
        }

        public static void assertArrayEquals(java.lang.String message, float[] expecteds, float[] actuals, float delta){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(message, expecteds, actuals, delta);
        }

        public static void assertArrayEquals(java.lang.String message, int[] expecteds, int[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(message, expecteds, actuals);
        }

        public static void assertArrayEquals(java.lang.String message, long[] expecteds, long[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(message, expecteds, actuals);
        }

        public static void assertArrayEquals(java.lang.String message, java.lang.Object[] expecteds, java.lang.Object[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(message, expecteds, actuals);
        }

        public static void assertArrayEquals(java.lang.String message, short[] expecteds, short[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertArrayEquals(message, expecteds, actuals); 
        }

        //@deprecated
        public static void assertEquals(double expected, double actual){
                if(assertsOn)
                       org.junit.Assert.assertEquals(expected, actual); 
        }

        public static void assertEquals(double expected, double actual, double delta){
                if(assertsOn)
                       org.junit.Assert.assertEquals(expected, actual, delta); 
        }

        public static void assertEquals(float expected, float actual, float delta){
                if(assertsOn)
                       org.junit.Assert.assertEquals(expected, actual, delta); 
        }

        public static void assertEquals(long expected, long actual){
                if(assertsOn)
                       org.junit.Assert.assertEquals(expected, actual); 
        }

        //@deprecated
        public static void assertEquals(java.lang.Object[] expecteds, java.lang.Object[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertEquals(expecteds, actuals);
        }

        public static void assertEquals(java.lang.Object expected, java.lang.Object actual){
                if(assertsOn)
                       org.junit.Assert.assertEquals(expected, actual); 
        }

        //@deprecated
        public static void assertEquals(java.lang.String message, double expected, double actual){
                if(assertsOn)
                       org.junit.Assert.assertEquals(message, expected, actual); 
        }

        public static void assertEquals(java.lang.String message, double expected, double actual, double delta){
                if(assertsOn)
                       org.junit.Assert.assertEquals(message, expected, actual, delta); 
        }

        public static void assertEquals(java.lang.String message, float expected, float actual, float delta){
                if(assertsOn)
                       org.junit.Assert.assertEquals(message, expected, actual, delta); 
        }

        public static void assertEquals(java.lang.String message, long expected, long actual){
                if(assertsOn)
                       org.junit.Assert.assertEquals(message, expected, actual); 
        }

        //@deprecated
        public static void assertEquals(java.lang.String message, java.lang.Object[] expecteds, java.lang.Object[] actuals){
                if(assertsOn)
                       org.junit.Assert.assertEquals(message, expecteds, actuals);
        }

        public static void assertEquals(java.lang.String message, java.lang.Object expected, java.lang.Object actual){
                if(assertsOn)
                       org.junit.Assert.assertEquals(message, expected, actual);
        }

        public static void assertFalse(boolean condition){
                if(assertsOn)
                       org.junit.Assert.assertFalse(condition);
        }

        public static void assertFalse(java.lang.String message, boolean condition){
                if(assertsOn)
                       org.junit.Assert.assertFalse(message, condition);
        }

        public static void assertNotEquals(double unexpected, double actual, double delta){
                if(assertsOn)
                       org.junit.Assert.assertNotEquals(unexpected, actual, delta);
        }

        public static void assertNotEquals(float unexpected, float actual, float delta){
                if(assertsOn)
                       org.junit.Assert.assertNotEquals(unexpected, actual, delta);
        }

        public static void assertNotEquals(long unexpected, long actual){
                if(assertsOn)
                       org.junit.Assert.assertNotEquals(unexpected, actual);
        }

        public static void assertNotEquals(java.lang.Object unexpected, java.lang.Object actual){
                if(assertsOn)
                       org.junit.Assert.assertNotEquals(unexpected, actual);
        }

        public static void assertNotEquals(String message, double unexpected, double actual, double delta){
                if(assertsOn)
                       org.junit.Assert.assertNotEquals(message, unexpected, actual, delta);
        }

        public static void assertNotEquals(String message, float unexpected, float actual, float delta){
                if(assertsOn)
                       org.junit.Assert.assertNotEquals(message, unexpected, actual, delta);
        }

        public static void assertNotEquals(String message, long unexpected, long actual){
                if(assertsOn)
                       org.junit.Assert.assertNotEquals(message, unexpected, actual);
        }

        public static void assertNotEquals(String message, java.lang.Object unexpected, java.lang.Object actual){
                if(assertsOn)
                       org.junit.Assert.assertNotEquals(message, unexpected, actual);
        }

        public static void assertNotNull(java.lang.Object object){
                if(assertsOn)
                       org.junit.Assert.assertNotNull(object); 
        }

        public static void assertNotNull(java.lang.String message, java.lang.Object object){
                if(assertsOn)
                       org.junit.Assert.assertNotNull(message, object);
        }

        public static void assertNotSame(java.lang.Object unexpected, java.lang.Object actual){
                if(assertsOn)
                       org.junit.Assert.assertNotSame(unexpected, actual); 
        }

	public static <T> void assertThat(String reason, T actual, Object matcher){
		if(assertsOn && matcher instanceof  org.hamcrest.Matcher)
		       org.junit.Assert.assertThat(reason, actual, (org.hamcrest.Matcher)matcher);
	}

	public static <T> void assertThat(T actual, Object matcher){
		if(assertsOn && matcher instanceof org.hamcrest.Matcher)
		       org.junit.Assert.assertThat(actual, (org.hamcrest.Matcher)matcher);
	}
        public static void assertNotSame(java.lang.String message, java.lang.Object unexpected, java.lang.Object actual){
                if(assertsOn)
                       org.junit.Assert.assertNotSame(message, unexpected, actual);
        }

        public static void assertNull(java.lang.Object object){
                if(assertsOn)
                       org.junit.Assert.assertNull(object);
        }

        public static void assertNull(java.lang.String message, java.lang.Object object){
                if(assertsOn)
                       org.junit.Assert.assertNull(message, object); 
        }

        public static void assertSame(java.lang.Object expected, java.lang.Object actual){
                if(assertsOn)
                       org.junit.Assert.assertSame(expected, actual);
        }

        public static void assertSame(java.lang.String message, java.lang.Object expected, java.lang.Object actual){
                if(assertsOn)
                       org.junit.Assert.assertSame(message, expected, actual);
        }


        public static void assertTrue(boolean condition){
                if(assertsOn)
                       org.junit.Assert.assertTrue(condition);
        }

        public static void assertTrue(java.lang.String message, boolean condition){
                if(assertsOn)
                       org.junit.Assert.assertTrue(message, condition);
        }

        public static void fail(){
                if(assertsOn)
                       org.junit.Assert.fail();
        }

        public static void fail(java.lang.String message){
                if(assertsOn)
                       org.junit.Assert.fail(message); 
        }


        public static void assertThrownBy(String sourceClass, Throwable t) throws AssertionError{
                if(assertsOn){
                        StackTraceElement el = t.getStackTrace()[0];
                        assertEquals(sourceClass, el.getClassName());
                }        
        }
}
