We do not distribute DaCapo, the jar has to be downloaded from `https://sourceforge.net/projects/dacapobench/files/9.12-bach/` and placed into `/misc/dacapo-9.12/dacapo-9.12-bach.jar`.

Then run `ant execute-all` to exercise all DaCapo projects, the  coverage reports will be generated in `/misc/dacapo-9.12/output/`.

run `ant execute-<name>` to exercise a specified dacapo project.

run `ant run-Dacapo-ReportAggregator` to aggregate coverage reports.

Note that this folder contains some redundant jars in `daytrader/`, this is to faciliate instrumentation of nested jars in DaCapo.