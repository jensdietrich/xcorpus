package nz.ac.massey.cs.xcorpus.featureanalysis;

/**
 * Simple listener printing results to the console, mainly for testing.
 * @author jens dietrich
 */
public class ConsoleAnalysisListener implements AnalysisListener {

    @Override
    public void featureFound(AnalysisContext context, Feature feature) {
        System.out.println("Found feature \"" + feature.getGroup() + " // " + feature.getName() + "\" in "  + context.getDataSet() + " // " + context.getProject() + " // " + context.getFile() + " // " + context.getClazz());
    }
}
