package nz.ac.massey.cs.xcorpus.commons;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Useful shared utils.
 * @author jens dietrich
 */
public class Utils {

    public static String relativise(File root, File f) {
        Path pathAbsolute = Paths.get(f.getAbsolutePath());
        Path pathBase = Paths.get(root.getAbsolutePath());
        return pathBase.relativize(pathAbsolute).toString();
    }
}
