package nz.ac.massey.cs.xcorpus.featureanalysis;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.io.PrintStream;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Renders analysis results into a data table.
 * @author jens dietrich
 */
public class AnalysisResultRenderer  {

    public static void render(Table<Program,Feature,Integer> dataTable, PrintStream out)  throws Exception {


        // split table in table per program
        Map<String,Table<Program,Feature,Integer>> dataTables = spilt(dataTable);
        String colSpec = "|ll|";
        for (int i=0;i<dataTables.size();i++) {
            colSpec=colSpec+"ll|";
        };

        out.println("\\begin{table}[]");
        out.println("\\footnotesize");
        out.println("\\centering");
        out.println("\\caption{Features in the corpus}");
        out.println("\\label{tab:features}");
        out.println("\\begin{tabular}{" + colSpec + "}");
        out.println("\t\\hline");
        // out.println("\tgroup & feature & programs & avr \\\\");

        // header row 1
        out.print("\t& ");
        for (String dataSet:dataTables.keySet()) {
            out.print("& \\multicolumn{2}{|c|}{");
            out.print(shortenDataSetName(sanitize(dataSet)));
            out.print("}");

        }
        out.println("\\\\");

        // header row 2
        out.print("\tgroup& feature ");
        for (String dataSet:dataTables.keySet()) {
            out.print("& count & avg ");

        }
        out.println("\\\\ \\hline");

        String group = null;
        for (Feature feature:Feature.values()) {

            if (group!=null && group!=feature.getGroup()) {
                // divider
                out.println("\t \\hline");
            }
            group = feature.getGroup();
            out.print("\t");
            out.print(shortenGroupName(sanitize(feature.getGroup())));
            out.print("\t&\t");
            out.print(shortenFeatureName(sanitize(feature.getName())));

            for (String dataSet:dataTables.keySet()) {
                Map<Program,Integer> counts = dataTables.get(dataSet).column(feature);
                String avg = computeAvg(counts.values());
                out.print("\t&\t");
                out.print(counts.size());
                out.print("\t&\t");
                out.print(avg);
            }

            out.println("\\\\ ");
        }

        out.println("\t\\hline");

        out.println("\\end{tabular}");
        out.println("\\end{table}");
    }

    private static String shortenFeatureName(String name) {
        return name.replace("callsite","c.s.")
                .replace("java.","\\$j.")
                .replace("javax.","\\$jx.")
                .replace(".lang.",".\\$l.")
                .replace(".reflect.",".\\$r.")
                .replace(".util.",".\\$u.")
                .replace("implementation of","impl. of")
                ;
    }

    private static String shortenGroupName(String name) {
        if (name.equals(Feature.SUBTYPE_OF_java_util_function__.getGroup())) {
            return "invokedyn.";
        }
        else {
            return name;
        }
    }

    private static String shortenDataSetName(String name) {
        name = name.substring(name.lastIndexOf("/")+1);
        if (name.contains("qualitas")) {
            return "qualit. corp.";
        }
        else if (name.contains("xcorpusplus")) {
            return "plus";
        }
        else {
            return name;
        }
    }


    private static String sanitize(String name) {
        return name.replace("#","\\#").replace("_","\\_");
    }

    private static String computeAvg(Collection<Integer> counts) {
        double dC = counts.size();
        double dS = 0.0;
        for (Integer v:counts) {
            dS = dS + v;
        }
        String avg = null;
        if (counts.size()==0) {
            return "n/a";
        }
        else {
            double a = dS / dC;
            return String.format("%.2f",a);
        }
    }

    private static Map<String,Table<Program,Feature,Integer>> spilt(Table<Program, Feature, Integer> dataTable) {
        Map<String,Table<Program,Feature,Integer>> dataTables = new LinkedHashMap<>();
        Collection<String> datasets = dataTable.rowKeySet().stream().map(p -> p.getDataset()).distinct().collect(Collectors.toList());
        for (String dataset:datasets) {
            dataTables.put(dataset, HashBasedTable.create());
        }
        for (Program program:dataTable.rowKeySet()) {
            Table<Program, Feature, Integer> table = dataTables.get(program.getDataset());
            Map<Feature,Integer> map = dataTable.row(program);
            for (Feature feature:map.keySet()) {
                table.put(program, feature, map.get(feature));
            }
        }
        return dataTables;
    }


}
