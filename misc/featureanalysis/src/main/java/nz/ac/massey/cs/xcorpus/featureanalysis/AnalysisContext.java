package nz.ac.massey.cs.xcorpus.featureanalysis;

/**
 * Context describing feature locations.
 * @author jens dietrich
 */
public class AnalysisContext {
    private String dataSet = null;
    private String project = null;
    private String file = null;
    private String clazz = null;

    public String getDataSet() {
        return dataSet;
    }

    public void setDataSet(String dataSet) {
        this.dataSet = dataSet;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AnalysisContext that = (AnalysisContext) o;

        if (dataSet != null ? !dataSet.equals(that.dataSet) : that.dataSet != null) return false;
        if (project != null ? !project.equals(that.project) : that.project != null) return false;
        if (file != null ? !file.equals(that.file) : that.file != null) return false;
        return clazz != null ? clazz.equals(that.clazz) : that.clazz == null;

    }

    @Override
    public int hashCode() {
        int result = dataSet != null ? dataSet.hashCode() : 0;
        result = 31 * result + (project != null ? project.hashCode() : 0);
        result = 31 * result + (file != null ? file.hashCode() : 0);
        result = 31 * result + (clazz != null ? clazz.hashCode() : 0);
        return result;
    }
}
