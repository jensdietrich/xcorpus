package nz.ac.massey.cs.xcorpus.featureanalysis;

/**
 * Interface for analysis callbacks.
 * @author jens dietrich
 */
public interface AnalysisListener {

    public void featureFound(AnalysisContext context, Feature feature);
}
