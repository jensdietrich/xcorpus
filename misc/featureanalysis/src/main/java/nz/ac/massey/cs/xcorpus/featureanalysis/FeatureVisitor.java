package nz.ac.massey.cs.xcorpus.featureanalysis;

import org.objectweb.asm.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Byte code visitor. Detects features, and registers them wth an analysis listener.
 * @author jens dietrich
 */
public class FeatureVisitor extends ClassVisitor {

    private AnalysisListener listener = null;
    private AnalysisContext context = null;

    public FeatureVisitor(AnalysisListener listener) {
        super(Opcodes.ASM5);
        this.listener = listener;
    }
    public AnalysisContext getContext() {
        return context;
    }

    public void setContext(AnalysisContext context) {
        this.context = context;
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version,access,name,signature,superName,interfaces);
        checkTypeFlag(access);
        checkExtends(superName);
        checkImplements(interfaces);
        checkSubtypes(superName,interfaces);
        if (signature!=null) {
            featureFound(Feature.GENERIC_TYPE);
        }
    }

    @Override
    public AnnotationVisitor visitAnnotation(String s, boolean b) {
        super.visitAnnotation(s, b);
        featureFound(Feature.USES_TYPE_ANNOTATION);
        return null;
    }

    @Override
    public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String s, boolean b) {
        super.visitTypeAnnotation(typeRef, typePath, s, b);
        featureFound(Feature.USES_TYPE_PARAMETER_ANNOTATION);
        return null;
    }

    @Override
    public MethodVisitor visitMethod(int flag, String name, String desc, String signature, String[] strings) {
        checkMethodFlag(flag);

        if (signature!=null && desc != signature) {
            featureFound(Feature.GENERIC_METHOD);
        }
        return new MethodVisitor(Opcodes.ASM5) {
            @Override
            public void visitInvokeDynamicInsn(String name, String descr, Handle handle, Object... objects) {
                super.visitInvokeDynamicInsn(name, descr, handle, objects);
                featureFound(Feature.CALLSITE_USING_invokedynamic);
            }

            @Override
            public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
                super.visitMethodInsn(opcode,owner,name,desc,itf);
                checkCallsite(opcode,owner,name,desc,itf);
                checkUsedType(owner);
            }

            @Override
            public void visitLocalVariable(String name, String desc, String signature, Label start, Label end, int index) {
                super.visitLocalVariable(name,desc,signature,start,end,index);
                if (signature!=null && desc != signature) {
                    featureFound(Feature.GENERIC_LOCALVARIABLE);
                }
                checkUsedType(extractType(desc));
            }

            @Override
            public AnnotationVisitor visitAnnotation(String s, boolean b) {
                featureFound(Feature.USES_METHOD_ANNOTATION);
                return super.visitAnnotation(s, b);
            }

            @Override
            public AnnotationVisitor visitTypeAnnotation(int i, TypePath typePath, String s, boolean b) {
                super.visitTypeAnnotation(i, typePath, s, b);
                featureFound(Feature.USES_TYPE_USE_ANNOTATION);
                return null;
            }

            @Override
            public AnnotationVisitor visitParameterAnnotation(int i, String s, boolean b) {
                return super.visitParameterAnnotation(i, s, b);
            }

            @Override
            public AnnotationVisitor visitAnnotationDefault() {
                return super.visitAnnotationDefault();
            }
        };
    }

    @Override
    public FieldVisitor visitField(final int access, final String name, final String desc, final String signature, final Object value) {
        super.visitField(access,name,desc,signature,value);
        if (signature!=null && desc != signature) {
            featureFound(Feature.GENERIC_FIELD);
        }
        checkUsedType(extractType(desc));
        return new FieldVisitor(Opcodes.ASM5) {
            @Override
            public AnnotationVisitor visitAnnotation(String s, boolean b) {
                super.visitAnnotation(s, b);
                featureFound(Feature.USES_FIELD_ANNOTATION);
                return null;
            }

            @Override
            public AnnotationVisitor visitTypeAnnotation(int i, TypePath typePath, String s, boolean b) {
                return super.visitTypeAnnotation(i, typePath, s, b);
            }
        };
    }

    // extract the type from descriptor
    private String extractType(String s) {
        if (s.startsWith("L") && s.endsWith(";")) {
            return s.substring(1,s.length()-1);
        }
        return s;
    }


    private void featureFound(Feature feature) {
        listener.featureFound(context,feature);
    }

    // rules
    private void checkExtends(String superName) {
        if (superName==null) return;

        if (superName.equals("java/lang/Thread")) {
            featureFound(Feature.EXTENDS_java_lang_Thread);
        }
    }
    private void checkImplements(String[] interfaces) {
        if (interfaces==null) return;
        for (String i : interfaces) {
            if (i.startsWith("java/lang/reflect/InvocationHandler")) {
                featureFound(Feature.IMPLEMENTS_java_lang_reflect_InvocationHandler);
            }
            else if (i.startsWith("java/lang/Runnable")) {
                featureFound(Feature.IMPLEMENTS_java_lang_Runnable);
            }
        }
    }

    private void checkSubtypes(String superName,String[] interfaces) {
        if (interfaces==null) return;

        List<String> superTypes = new ArrayList<>();
        if (superName!=null) superTypes.add(superName);
        if (interfaces!=null) {
            for (String i:interfaces) superTypes.add(i);
        }
        for (String t : interfaces) {
            if (t.startsWith("java/util/function")) {
                featureFound(Feature.SUBTYPE_OF_java_util_function__);
            }
        }
    }

    private void checkCallsite(int opcode, String owner, String name, String desc, boolean itf) {
        if (owner.equals("java/lang/reflect/Method") && name.equals("invoke")) {
            featureFound(Feature.CALLSITE_FOR_java_lang_reflect_Method__invoke);
        }
        else if (owner.equals("java/lang/reflect/Constructor") && name.equals("newInstance")) {
            featureFound(Feature.CALLSITE_FOR_java_lang_reflect_Constructor__newInstance);
        }
        else if (owner.equals("java/lang/reflect/Field")) {
            if (name.equals("get") || name.equals("getBoolean") || name.equals("getByte") || name.equals("getChar") ||
                    name.equals("getDouble") || name.equals("getFloat") || name.equals("getInt") || name.equals("getLong") || name.equals("getShort") ){
                featureFound(Feature.CALLSITE_FOR_java_lang_reflect_Field__get);
            }
            else if (name.equals("set") || name.equals("setBoolean") || name.equals("setByte") || name.equals("setChar") ||
                    name.equals("setDouble") || name.equals("setFloat") || name.equals("setInt") || name.equals("setLong") || name.equals("setShort") ){
                featureFound(Feature.CALLSITE_FOR_java_lang_reflect_Field__set);
            }
        }
        else if (owner.equals("java/lang/reflect/Proxy") && name.equals("newProxyInstance")) {
            featureFound(Feature.CALLSITE_FOR_java_lang_reflect_Proxy__newProxyInstance);
        }
        else if (owner.equals("java/lang/Thread") && name.equals("<init>")) {
            featureFound(Feature.CALLSITE_FOR_java_lang_Thread__);
        }
        else if (owner.equals("java/util/concurrent/Executors")) {
            featureFound(Feature.CALLSITE_FOR_java_util_concurrent_Executors__);
        }
        else if (owner.equals("java/lang/Class") && name.equals("newInstance")) {
            featureFound(Feature.CALLSITE_FOR_java_lang_Class__newInstance);
        }
        else if (owner.equals("java/beans/Introspector")) {
            featureFound(Feature.CALLSITE_FOR_java_beans_Introspector__);
        }
        else if (owner.equals("java/util/ServiceLoader")) {
            featureFound(Feature.CALLSITE_FOR_java_util_ServiceLoader__);
        }
        else if (owner.equals("java/lang/Runtime")) {
            featureFound(Feature.CALLSITE_FOR_java_lang_Runtime__);
        }
        else if (owner.equals("java/lang/ref/WeakReference")) {
            featureFound(Feature.CALLSITE_FOR_java_lang_ref_WeakReference__);
        }
        else if (owner.equals("java/lang/ref/SoftReference")) {
            featureFound(Feature.CALLSITE_FOR_java_lang_ref_SoftReference__);
        }
        else if (owner.equals("java/lang/ref/PhantomReference")) {
            featureFound(Feature.CALLSITE_FOR_java_lang_ref_PhantomReference__);
        }
        else if (owner.equals("java/util/WeakHashMap")) {
            featureFound(Feature.CALLSITE_FOR_java_util_WeakHashMap__);
        }
        else if (owner.startsWith("java/util/function/")) {
            featureFound(Feature.CALLSITE_FOR_java_util_function__);
        }
        else if (owner.startsWith("java/lang/invoke/")) {
            featureFound(Feature.CALLSITE_FOR_java_util_invoke__);
        }
        else if (owner.equals("sun/misc/Unsafe")) {
            featureFound(Feature.CALLSITE_FOR_sun_misc_Unsafe__);
        }
        else if (owner.equals("java/io/ObjectInputStream")) {
            featureFound(Feature.CALLSITE_FOR_java_io_ObjectInputStream__);
        }
        else if (owner.equals("java/beans/XMLDecoder")) {
            featureFound(Feature.CALLSITE_FOR_java_beans_XMLDecoder__);
        }
        else if (owner.equals("javax/tools/JavaCompiler")) {
            featureFound(Feature.CALLSITE_FOR_javax_tools_JavaCompiler_);
        }
        else if (owner.equals("javax/tools/ToolProvider")) {
            featureFound(Feature.CALLSITE_FOR_javax_tools_ToolProvider_);
        }
        else if (owner.startsWith("javax/script/")) {
            featureFound(Feature.CALLSITE_FOR_javax_script__);
        }
        else if (owner.equals("java/lang/ClassLoader")) {
            featureFound(Feature.CALLSITE_FOR_java_lang_ClassLoader__);
        }
        else if (owner.equals("java/security/SecureClassLoader")) {
            featureFound(Feature.CALLSITE_FOR_java_security_SecureClassLoader__);
        }
        else if (owner.equals("java/net/URLClassLoader")) {
            featureFound(Feature.CALLSITE_FOR_java_net_URLClassLoader__);
        }
        else if (owner.equals("java/rmi/server/RMIClassLoader")) {
            featureFound(Feature.CALLSITE_FOR_java_rmi_server_RMIClassLoader__);
        }

    }

    private void checkUsedType(String typeName) {
        if (typeName==null) return;
    }

    private void checkMethodFlag(int flag) {
        // careful: multiple flags might be set, so don't use else if
        if (checkFlag(flag, Opcodes.ACC_NATIVE)) {
            featureFound(Feature.DECLARES_NATIVE_METHOD);
        }
        if (checkFlag(flag, Opcodes.ACC_SYNTHETIC)) {
            featureFound(Feature.DECLARES_SYNTHETIC_METHOD);
        }
        if (checkFlag(flag, Opcodes.ACC_BRIDGE)) {
            featureFound(Feature.DECLARES_BRIDGE_METHOD);
        }
    }

    private void checkTypeFlag(int flag) {
        // careful: multiple flags might be set, so don't use else if
        if (checkFlag(flag, Opcodes.ACC_ANNOTATION)) {
            featureFound(Feature.DECLARES_ANNOTATION);
        }
    }

    // usage: checkFlag(access,Opcodes.ACC_ANNOTATION)
    private static boolean checkFlag (int flags, int opCode) {
        return (flags & opCode) == opCode;
    }
}
