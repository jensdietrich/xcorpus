package nz.ac.massey.cs.xcorpus.testcounter;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.google.common.base.Preconditions;
import org.objectweb.asm.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static com.google.common.io.Files.simplifyPath;

/**
 * A script to traverse the projects in the corpus in order to count built-in and generated tests.
 * Uses source code analysis to count generated tests, and bytecode analysis to count builit-in tests.
 * Potenial accuracy issues:
 * (1) we only look for @Test annotations in generated tests, assuming that evosuite only generates junit4 tests and imports the respective class
 * (2) we look for junit4 and junit3 for built-in tests, if no junit4 test methods are found, we look for junit3 methods as public methods starting
 * with test* with a descriptor ()V. We do not analyse the classhierarchy, we inspected some classes and couldn't find any false positives but at least in
 * theory it is possible that this leads to over-approximation.
 * @author jens dietrich
 */
public class CountTests {

    // for source code analysis
    static class SourceCodeJUnit4MethodCounter extends VoidVisitorAdapter<Object> {
        private AtomicInteger methodCounter = null;
        public SourceCodeJUnit4MethodCounter(AtomicInteger methodCounter) {
            super();
            this.methodCounter = methodCounter;
        }
        @Override
        public void visit(MethodDeclaration n, Object arg) {
            super.visit(n, arg);

            // look for @Test annotation , do not resolve package names, unlikely but possible source of FPs
            // note that evosuite seems to use local @Test annos
            for (AnnotationExpr ann:n.getAnnotations()) {
                if (ann.getName().getName().equals("Test") || ann.getName().getName().equals("org.junit.Test")) {
                    methodCounter.incrementAndGet();
                    break;
                }
            }
        }
    };

    static class ByteCodeJUnit4MethodCounter extends ClassVisitor {
        private AtomicInteger methodCounter = null;
        public ByteCodeJUnit4MethodCounter(AtomicInteger methodCounter) {
            super(Opcodes.ASM5);this.methodCounter = methodCounter;

        }
        @Override
        public MethodVisitor visitMethod(int i, String s, String s1, String s2, String[] strings) {
            return new MethodVisitor(Opcodes.ASM5) {
                @Override
                public AnnotationVisitor visitAnnotation(String s, boolean b) {
                    if (s.equals("Lorg/junit/Test;")) {
                        methodCounter.incrementAndGet();
                    }
                    return null;
                }
            };
        }
    }

    static class ByteCodeJUnit3MethodCounter extends ClassVisitor {
        private AtomicInteger methodCounter = null;
        public ByteCodeJUnit3MethodCounter(AtomicInteger methodCounter) {
            super(Opcodes.ASM5);this.methodCounter = methodCounter;
        }
        @Override
        public MethodVisitor visitMethod(int access,String name,String desc,String signature,String[] exceptions) {
            boolean isPublic = (access|Opcodes.ACC_PUBLIC)==1;
            if (name.startsWith("test") && desc.equals("()V") && isPublic) {
                methodCounter.incrementAndGet();
            }
            return null;
        }
    }

    public static void main (String[] args) throws Exception {
        Preconditions.checkArgument(args.length>0,CountTests.class.getSimpleName() + "#main must be called with one argument to specify data root folder");
        File dataRoot = new File(args[0]);
        Preconditions.checkArgument(dataRoot.exists(),"Data root folder " + args[0] + "does not exist");

        AtomicInteger unparsedFiles = new AtomicInteger(0);
        AtomicInteger generatedTestClassesCount = new AtomicInteger(0);
        AtomicInteger generatedTestMethodsCount = new AtomicInteger(0);
        AtomicInteger builtinTestClassesCount = new AtomicInteger(0);
        AtomicInteger builtinTestMethodsCount = new AtomicInteger(0);
        List<String> projectsWithBuiltInTests = new ArrayList<>();

        for (File dataset:dataRoot.listFiles(child -> child.isDirectory())) {
            for (File program:dataset.listFiles(child2->!child2.getName().startsWith("."))) {

                System.out.println("Analysing " + program.getName());

                // extract locations from ant
                File ant = new File(program,".xcorpus/exercise.xml");
                assert ant.exists();

                // evosuite tests
                String generatedTestSources = findAntProperty(ant,"evosuite-tests.src.dir");
                generatedTestSources = generatedTestSources.replace("${basedir}",program.getAbsolutePath()+"/.xcorpus");
                generatedTestSources = generatedTestSources + ".zip";
                generatedTestSources = simplifyPath(generatedTestSources);


                File zip = new File(generatedTestSources);
                if (zip.exists()) {
                    System.out.println("\tfound generated test sources: " + zip.getAbsolutePath());
                    countAllUsingSourceCodeAnalysis(new ZipFile(zip),generatedTestClassesCount,generatedTestMethodsCount,unparsedFiles, cl -> cl.endsWith("ESTest.java"));
                }
                else {
                    System.err.println("\tNo builtin tests found, file does not exist: " + zip.getAbsolutePath());
                }

                // built-in tests
                String  builtInTestSources = program.getAbsolutePath() + "/project/builtin-tests.zip";
                builtInTestSources = simplifyPath(builtInTestSources);
                zip = new File(builtInTestSources);
                if (zip.exists()) {
                    System.out.println("\tfound builtin test classes:   " + zip.getAbsolutePath());
                    projectsWithBuiltInTests.add(program.getName());
                    countAllUsingByteCodeAnalysis(new ZipFile(zip),builtinTestClassesCount,builtinTestMethodsCount,unparsedFiles, cl -> cl.endsWith(".class"));
                }
            }
        }

        System.out.println();
        System.out.println("Generated test classes found:    " + generatedTestClassesCount);
        System.out.println("Generated test methods found:    " + generatedTestMethodsCount);
        System.out.println("Built-in test classes found :    " + builtinTestClassesCount);
        System.out.println("Built-in test methods found :    " + builtinTestMethodsCount);
        System.out.println("Projects with built-in tests:    " + projectsWithBuiltInTests.size());
        System.out.println("Unparsable classes:              " + unparsedFiles);
        System.out.println("\tNOTE: aspects, scripts in other JVM languages etc");
        System.out.println();
        System.out.println("Projects with built-in tests: ");
        System.out.print("\t");
        for (String p:projectsWithBuiltInTests) {
            System.out.print(p);
            System.out.print(" ");
        }
        System.out.println();
    }

    private static void countAllUsingSourceCodeAnalysis(ZipFile container, AtomicInteger testClassesCount, AtomicInteger testMethodsCount, AtomicInteger unparsedFiles, Predicate<String> classNameFilter) {
        Enumeration<? extends ZipEntry> en = container.entries();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (name.endsWith(".java") && classNameFilter.test(name)) {
                try (InputStream in = container.getInputStream(e)) {
                    CompilationUnit cu = JavaParser.parse(in);
                    if (countUsingSourceCodeAnalysis(cu,testClassesCount,testMethodsCount)) {
                        testClassesCount.incrementAndGet();
                    }
                }
                catch (Exception x) {
                    unparsedFiles.incrementAndGet();
                    System.err.println("\tfailed to parse: " + container + "#" + e.getName());
                }
            }
        }
    }

    private static boolean countUsingSourceCodeAnalysis(CompilationUnit cu, AtomicInteger testClassesCount, AtomicInteger testMethodsCount) throws Exception {
        testClassesCount.incrementAndGet();

        int oldMethodCounter = testMethodsCount.get();
        SourceCodeJUnit4MethodCounter visitor = new SourceCodeJUnit4MethodCounter(testMethodsCount);
        visitor.visit(cu, null);

        return oldMethodCounter<testMethodsCount.get();
    }

    private static Document parseXML(File xml) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(xml);
    }

    private static void countAllUsingByteCodeAnalysis(ZipFile container, AtomicInteger testClassesCount, AtomicInteger testMethodsCount, AtomicInteger unparsedFiles, Predicate<String> classNameFilter) {
        Enumeration<? extends ZipEntry> en = container.entries();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (name.endsWith(".class") && classNameFilter.test(name)) {
                try (InputStream in = container.getInputStream(e)) {
                    ClassReader clReader = new ClassReader(in);
                    boolean testsFound = countJunit4TestsUsingByteCodeAnalysis(clReader,testClassesCount,testMethodsCount);
                    if (!testsFound) {
                        testsFound = countJunit3TestsUsingByteCodeAnalysis(clReader,testClassesCount,testMethodsCount);
                    }
                    if (testsFound) {
                        testClassesCount.incrementAndGet();
                    }
                }
                catch (Exception x) {
                    unparsedFiles.incrementAndGet();
                    System.err.println("\tfailed to parse: " + container + "#" + e.getName());
                }
            }
        }
    }

    private static boolean countJunit4TestsUsingByteCodeAnalysis(ClassReader clReader, AtomicInteger testClassesCount, AtomicInteger testMethodsCount) {
        int before = testMethodsCount.get();
        clReader.accept(new ByteCodeJUnit4MethodCounter(testMethodsCount),0);
        return before<testMethodsCount.get();
    }

    private static boolean countJunit3TestsUsingByteCodeAnalysis(ClassReader clReader, AtomicInteger testClassesCount, AtomicInteger testMethodsCount) {
        int before = testMethodsCount.get();
        clReader.accept(new ByteCodeJUnit3MethodCounter(testMethodsCount),0);
        return before<testMethodsCount.get();
    }

    private static String findAntProperty(File xml, String name) throws Exception {
        Document doc = parseXML(xml);
        Element root = doc.getDocumentElement();
        NodeList list = root.getElementsByTagName("property");
        for (int i=0;i<list.getLength();i++) {
            Element node = (Element)list.item(i);
            String propName = node.getAttribute("name");
            if (propName.equals(name)) {
                return node.getAttribute("location");
            }
        }
        assert false;
        return null;
    }


}
