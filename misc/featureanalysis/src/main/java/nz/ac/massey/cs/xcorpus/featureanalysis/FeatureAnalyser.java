package nz.ac.massey.cs.xcorpus.featureanalysis;

import com.google.common.base.Preconditions;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.collect.TreeTraverser;
import com.google.common.io.Files;
import org.objectweb.asm.ClassReader;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static nz.ac.massey.cs.xcorpus.commons.Utils.relativise;

/**
 * Executable feature analysis. Performs analysis on bytecode.
 * @author jens dietrich
 */
public class FeatureAnalyser {

    public static void main(String[] args) throws Exception {
        Preconditions.checkArgument(args.length>0,FeatureAnalyser.class.getSimpleName() + "#main must be called with one argument to specify data root folder");
        File outputFolder = new File("output");
        if (!outputFolder.exists()) {
            outputFolder.mkdirs();
            System.out.println("Created output folder: " + outputFolder);
        }
        for (String folder:args) {
            Preconditions.checkState(new File(folder).exists(), "Datafolder " + args[0] + " does not exist");
        }

        File detailsFile = new File(outputFolder,"analysis-results-details.csv");
        if (detailsFile.exists()) {
            System.out.println("Deleting old details file: " + detailsFile);
            detailsFile.delete();
        }

        Table<Program,Feature,Integer> dataTable = HashBasedTable.create();

        for (String folder:args) {
            System.out.println("Analysing: dataset " + folder);
            AnalysisResultCollector collector = new AnalysisResultCollector(dataTable,detailsFile);
            analyse(folder, collector);
        }

        File resultFile = new File(outputFolder,"analysis-results.tex");
        try (PrintStream out = new PrintStream(new FileOutputStream(resultFile))){
            AnalysisResultRenderer.render(dataTable, out);
            System.out.println("Analysis result summary (latex formatted) written to " + resultFile);
            System.out.println("Details (csv) written to " + detailsFile);
        }
    }

    public static void analyse(String root,AnalysisListener listener) throws Exception {
        Preconditions.checkState(new File(root).exists());
        File[] projects = new File(root).listFiles(f -> f.isDirectory());
        TreeTraverser<File> traverser = Files.fileTreeTraverser();
        for (File project:projects) {
            System.out.println("Analysing: " + relativise(new File(root).getParentFile(),project));
            FluentIterable<File> iter = traverser.preOrderTraversal(project);
            for (File f: iter) {
                String name = f.getName();
                if (name.endsWith("bin.zip")) {
                    String projectName = f.getParentFile().getParentFile().getName();
                    String dataSetName = f.getParentFile().getParentFile().getParentFile().getName();
                    analyse(new ZipFile(f),listener,dataSetName,projectName,relativise(project,f));
                }
            }
        }
    }



    private static void analyse (ZipFile zip,AnalysisListener listener,String dataSet,String projectName, String fileName) throws IOException {
        AnalysisContext context = new AnalysisContext();
        context.setProject(projectName);
        context.setDataSet(dataSet);
        context.setFile(fileName);
        Enumeration<? extends ZipEntry> en = zip.entries();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (name.endsWith(".class")) {
                InputStream in = zip.getInputStream(e);
                context.setClazz(name.substring(0,name.length()-6).replace('/','.'));
                doAnalyse(in,context,listener);
                in.close();
            }
        }
    }

    // public to unit test
    public static void doAnalyse(InputStream in,AnalysisContext context,AnalysisListener listener) throws IOException{
        FeatureVisitor visitor = new FeatureVisitor(listener);
        visitor.setContext(context);
        new ClassReader(in).accept(visitor, 0);
    }


}
