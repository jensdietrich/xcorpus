package nz.ac.massey.cs.xcorpus.featureanalysis;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Collects analysis results in a table for later statistical analysis.
 * @author jens dietrich
 */
public class AnalysisResultCollector implements AnalysisListener {


    private Table<Program,Feature,Integer> dataTable = HashBasedTable.create();
    private File detailsFile = null;

    public AnalysisResultCollector(Table<Program, Feature, Integer> dataTable, File detailsFile) {
        this.dataTable = dataTable;
        this.detailsFile = detailsFile;
    }

    public Table<Program, Feature, Integer> getDataTable() {
        return dataTable;
    }

    @Override
    public void featureFound(AnalysisContext context, Feature feature) {
        Program program = new Program(context.getDataSet(),context.getProject()+"/"+context.getFile());
        Integer count = dataTable.get(program,feature);
        int c = count==null?0:count;
        dataTable.put(program,feature,c+1);

        // log into details file
        try (PrintStream out = new PrintStream(new FileOutputStream(detailsFile,true))) {
            out.print(feature.getGroup());
            out.print("\t");
            out.print(feature.getName());
            out.print("\t");
            out.print(context.getDataSet());
            out.print("\t");
            out.print(context.getProject());
            out.print("\t");
            out.print(context.getFile());
            out.print("\t");
            out.println(context.getClazz());
        }
        catch (IOException x) {
            x.printStackTrace();
        }
    }
}
