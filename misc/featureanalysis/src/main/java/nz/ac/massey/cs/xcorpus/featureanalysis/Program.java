package nz.ac.massey.cs.xcorpus.featureanalysis;

/**
 * Simple data structure to represent elements of the dataset.
 * @author jens dietrich
 */
public class Program {
    private String name = null;
    private String dataset = null;

    public Program(String dataset,String name) {
        this.name = name;
        this.dataset = dataset;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataset() {
        return dataset;
    }

    public void setDataset(String dataset) {
        this.dataset = dataset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Program program = (Program) o;

        if (name != null ? !name.equals(program.name) : program.name != null) return false;
        return dataset != null ? dataset.equals(program.dataset) : program.dataset == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (dataset != null ? dataset.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Program{" + dataset + " // " + name;
    }
}
