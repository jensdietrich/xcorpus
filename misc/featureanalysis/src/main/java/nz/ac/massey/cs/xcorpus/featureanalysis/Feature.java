package nz.ac.massey.cs.xcorpus.featureanalysis;

/**
 * Feature definitions.
 * @author jens dietrich
 */
public enum Feature {

    // proxies
    IMPLEMENTS_java_lang_reflect_InvocationHandler("proxies","implementation of java.lang.reflect.InvocationHandler"),
    CALLSITE_FOR_java_lang_reflect_Proxy__newProxyInstance("proxies","callsite of java.lang.reflect.Proxy#newProxyInstance"),

    // reflection
    CALLSITE_FOR_java_lang_reflect_Method__invoke("reflection","callsite of java.lang.reflect.Method#invoke"),
    CALLSITE_FOR_java_lang_reflect_Constructor__newInstance("reflection","callsite of java.lang.reflect.Constructor#newInstance"),
    CALLSITE_FOR_java_lang_reflect_Field__get("reflection","callsite of java.lang.reflect.Field#get*"),
    CALLSITE_FOR_java_lang_reflect_Field__set("reflection","callsite of java.lang.reflect.Field#set*"),
    CALLSITE_FOR_java_lang_Class__newInstance("reflection","callsite of java.lang.Class#newInstance"),
    CALLSITE_FOR_java_beans_Introspector__("reflection","callsite of java.beans.Introspector#*"),
    CALLSITE_FOR_java_util_ServiceLoader__("reflection","callsite of java.util.ServiceLoader#*"),
    CALLSITE_FOR_java_io_ObjectInputStream__("reflection","callsite of java.io.ObjectInputStream#*"),
    CALLSITE_FOR_java_beans_XMLDecoder__("reflection","callsite of java.beans.XMLDecoder#*"),

    // classloading
    CALLSITE_FOR_java_lang_ClassLoader__("classloading","callsite of java.lang.ClassLoader#*"),
    CALLSITE_FOR_java_security_SecureClassLoader__("classloading","callsite of java.security.SecureClassLoader#*"),
    CALLSITE_FOR_java_net_URLClassLoader__("classloading","callsite of java.net.URLClassLoader#*"),
    CALLSITE_FOR_java_rmi_server_RMIClassLoader__("classloading","callsite of java.rmi.server.RMIClassLoader#*"),

    // lambdas and invokedynamic
    // TODO: annotated with functional
    SUBTYPE_OF_java_util_function__("invokedynamic","subtype of any type in java.util.function"),
    CALLSITE_FOR_java_util_function__("invokedynamic","callsite of java.util.function.*#*"),
    CALLSITE_FOR_java_util_invoke__("invokedynamic","callsite of java.util.invoke.*#*"),
    CALLSITE_USING_invokedynamic("invokedynamic","invokedynamic callsite"),

    // generics
    GENERIC_METHOD("generics","generic method signature"),
    GENERIC_TYPE("generics","generic type signature"),
    GENERIC_FIELD("generics","generic field signature"),
    GENERIC_LOCALVARIABLE("generics","generic local variable signature"),

    // dynamic language
    CALLSITE_FOR_javax_tools_JavaCompiler_("dynlang","callsite of javax.tools.JavaCompiler#*"),
    CALLSITE_FOR_javax_tools_ToolProvider_("dynlang","callsite of javax.tools.ToolProvider#*"),
    CALLSITE_FOR_javax_script__("dynlang","callsite of javax.script.*#*"),

    // references
    CALLSITE_FOR_java_lang_ref_WeakReference__("reference","callsite of java.lang.ref.WeakReference#*"),
    CALLSITE_FOR_java_lang_ref_SoftReference__("reference","callsite of java.lang.ref.SoftReference#*"),
    CALLSITE_FOR_java_lang_ref_PhantomReference__("reference","callsite of java.lang.ref.PhantomReference#*"),
    CALLSITE_FOR_java_util_WeakHashMap__("reference","callsite of java.util.WeakHashMap#*"),

    // concurrency
    CALLSITE_FOR_java_lang_Thread__("threads","callsite of java.lang.Thread#*"),
    EXTENDS_java_lang_Thread("threads","subclass of java.lang.Thread"),
    IMPLEMENTS_java_lang_Runnable("threads","implementation of java.lang.Runnable"),
    CALLSITE_FOR_java_util_concurrent_Executors__("threads","callsite of java.util.concurrent.Executors#*"),

    // annotations
    DECLARES_ANNOTATION("annotation","declares annotation"),
    USES_TYPE_ANNOTATION("annotation","uses type annotation"),
    USES_FIELD_ANNOTATION("annotation","uses field annotation"),
    USES_METHOD_ANNOTATION("annotation","uses method annotation"),
    USES_TYPE_USE_ANNOTATION("annotation","uses type use annotation"),
    USES_TYPE_PARAMETER_ANNOTATION("annotation","uses type parameter annotation"),
    // local variables are not retained http://www.eclipse.org/aspectj/doc/released/adk15notebook/annotations.html
    // USES_LOCAL_VARIABLE_ANNOTATION("annotation","uses local variable annotation"),

    // system
    DECLARES_NATIVE_METHOD("system","native method definition"),
    CALLSITE_FOR_java_lang_Runtime__("system","callsite of java.lang.Runtime#*"),
    CALLSITE_FOR_sun_misc_Unsafe__("system","callsite of sun.misc.Unsafe#*"),

    // misc
    DECLARES_SYNTHETIC_METHOD("misc","synthetic method definition"),
    DECLARES_BRIDGE_METHOD("misc","bridge method definition");

    private String group;
    private String name;

    Feature(String group, String name) {
        this.group = group;
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public String getName() {
        return name;
    }
}
