package test.nz.ac.massey.cs.xcorpus.stats;

import com.google.common.base.Preconditions;
import nz.ac.massey.cs.xcorpus.featureanalysis.AnalysisContext;
import nz.ac.massey.cs.xcorpus.featureanalysis.Feature;
import nz.ac.massey.cs.xcorpus.featureanalysis.FeatureAnalyser;
import org.junit.Test;
import testdata.*;

import java.io.File;
import java.io.FileInputStream;
import static junit.framework.TestCase.assertTrue;

/**
 * JUnit tests.
 * @author jens dietrich
 */
public class Tests {

    private void test(Class<?> clazz, Feature oracle) throws Exception {
        File bytecode = new File("target/test-classes/" + clazz.getName().replace(".","/")+".class");
        Preconditions.checkState(bytecode.exists());

        AnalysisContext context = new AnalysisContext();
        context.setDataSet("tests");
        context.setProject(Tests.class.getName());
        context.setFile(bytecode.getAbsolutePath());

        TestAnalysisListener listener = new TestAnalysisListener();

        try (FileInputStream in = new FileInputStream(bytecode)) {
            FeatureAnalyser.doAnalyse(in,context,listener);
            in.close();
        }

        assertTrue(listener.foundFeatures.contains(oracle));
    }


    @Test
    public void testExtendsJavaUtilFunctionPredicate() throws Exception {
        test(ExtendsJavaUtilFunctionPredicate.class, Feature.SUBTYPE_OF_java_util_function__);
    }

    @Test
    public void testUsesJavaUtilFunctionPredicate() throws Exception {
        test(UsesFunction.class,Feature.CALLSITE_FOR_java_util_function__);
    }

    @Test
    public void testUsesJavaLangInvokeMethodHandle() throws Exception {
        test(UsesMethodHandle.class,Feature.CALLSITE_FOR_java_util_invoke__);
    }

    @Test
    public void testInvokeDynamic() throws Exception {
        test(UsesLambda.class, Feature.CALLSITE_USING_invokedynamic);
    }

    @Test
    public void testReflectiveMethodInvocation() throws Exception {
        test(ReflectiveCall.class, Feature.CALLSITE_FOR_java_lang_reflect_Method__invoke);
    }

    @Test
    public void testProxy1() throws Exception {
        test(UsesProxy.Handler.class,Feature.IMPLEMENTS_java_lang_reflect_InvocationHandler);
    }

    @Test
    public void testProxy2() throws Exception {
        test(UsesProxy.class,Feature.CALLSITE_FOR_java_lang_reflect_Proxy__newProxyInstance);
    }

    @Test
    public void testExtendsThread() throws Exception {
        test(ExtendsThread.class,Feature.EXTENDS_java_lang_Thread);
    }

    @Test
    public void testUsesThread1() throws Exception {
        test(InstantiatesThread1.class,Feature.CALLSITE_FOR_java_lang_Thread__);
    }

    @Test
    public void testUsesThread2() throws Exception {
        test(InstantiatesThread2.class,Feature.CALLSITE_FOR_java_lang_Thread__);
    }

    @Test
    public void testImplementsRunnable() throws Exception {
        test(ImplementsRunnable.class,Feature.IMPLEMENTS_java_lang_Runnable);
    }

    @Test
    public void testUsesExecutors() throws Exception {
        test(UsesExecutors.class,Feature.CALLSITE_FOR_java_util_concurrent_Executors__);
    }

    @Test
    public void testDeclaresSyntheticMethod() throws Exception {
        test(UsesCovariantReturnType.class,Feature.DECLARES_SYNTHETIC_METHOD);
    }

    @Test
    public void testDeclaresBridgeMethod() throws Exception {
        test(UsesCovariantReturnType.class,Feature.DECLARES_BRIDGE_METHOD);
    }

    @Test
    public void testUsesIntrospector() throws Exception {
        test(UsesIntrospector.class,Feature.CALLSITE_FOR_java_beans_Introspector__);
    }

    @Test
    public void testUsesServiceLoader() throws Exception {
        test(UsesServiceLoader.class,Feature.CALLSITE_FOR_java_util_ServiceLoader__);
    }

    @Test
    public void testUsesRuntime() throws Exception {
        test(UsesRuneTimeExec.class,Feature.CALLSITE_FOR_java_lang_Runtime__);
    }

    @Test
    public void testUsesSoftReference() throws Exception {
        test(UsesSoftReference.class,Feature.CALLSITE_FOR_java_lang_ref_SoftReference__);
    }

    @Test
    public void testUsesWeakReference() throws Exception {
        test(UsesWeakReference.class,Feature.CALLSITE_FOR_java_lang_ref_WeakReference__);
    }

    @Test
    public void testUsesPhantomReference() throws Exception {
        test(UsesPhantomReference.class,Feature.CALLSITE_FOR_java_lang_ref_PhantomReference__);
    }

    @Test
    public void testUsesWeakHashMap() throws Exception {
        test(UsesWeakHashMap.class,Feature.CALLSITE_FOR_java_util_WeakHashMap__);
    }

    @Test
    public void testDeclaresAnnotation() throws Exception {
        test(AnAnnotation.class,Feature.DECLARES_ANNOTATION);
    }

    @Test
    public void testUsesTypeAnnotation() throws Exception {
        test(AnAnnotation.class,Feature.USES_TYPE_ANNOTATION);
    }

    @Test
    public void testUsesMethodAnnotation() throws Exception {
        test(UsesAnnotations.class,Feature.USES_METHOD_ANNOTATION);
    }

    @Test
    public void testUsesFieldAnnotation() throws Exception {
        test(UsesAnnotations.class,Feature.USES_FIELD_ANNOTATION);
    }

    @Test
    public void testUsesTypeParameterAnnotation() throws Exception {
        test(UsesAnnotations.class,Feature.USES_TYPE_PARAMETER_ANNOTATION);
    }

    @Test
    public void testUsesTypeUseAnnotation() throws Exception {
        test(UsesAnnotations.class,Feature.USES_TYPE_USE_ANNOTATION);
    }

    @Test
    public void testUsesGenericMethod1() throws Exception {
        test(UsesGenericMethod1.class,Feature.GENERIC_METHOD);
    }

    @Test
    public void testUsesGenericMethod2() throws Exception {
        test(UsesGenericMethod2.class,Feature.GENERIC_METHOD);
    }

    @Test
    public void testUsesGenericField() throws Exception {
        test(UsesGenericField.class,Feature.GENERIC_FIELD);
    }

    @Test
    public void testUsesGenericLocalVariable() throws Exception {
        test(UsesGenericLocalVariable.class,Feature.GENERIC_LOCALVARIABLE);
    }

    @Test
    public void testUsesGenericType1() throws Exception {
        test(UsesGenericType1.class,Feature.GENERIC_TYPE);
    }

    @Test
    public void testUsesGenericType2() throws Exception {
        test(UsesGenericType2.class,Feature.GENERIC_TYPE);
    }

    @Test
    public void testUsesGenericType3() throws Exception {
        test(UsesGenericType3.class,Feature.GENERIC_TYPE);
    }

    @Test
    public void testUsesGenericType4() throws Exception {
        test(UsesGenericType4.class,Feature.GENERIC_TYPE);
    }

    @Test
    public void testUsesUnsafe() throws Exception {
        test(UsesUnsafe.class,Feature.CALLSITE_FOR_sun_misc_Unsafe__);
    }

    @Test
    public void testUsesReflectiveInstantiation1() throws Exception {
        test(ReflectiveInstantiation1.class,Feature.CALLSITE_FOR_java_lang_reflect_Constructor__newInstance);
    }

    @Test
    public void testUsesReflectiveInstantiation2() throws Exception {
        test(ReflectiveInstantiation2.class,Feature.CALLSITE_FOR_java_lang_Class__newInstance);
    }

    @Test
    public void testUsesBinaryDeserialisation() throws Exception {
        test(UsesBinDeserialisation.class,Feature.CALLSITE_FOR_java_io_ObjectInputStream__);
    }

    @Test
    public void testUsesXMLDeserialisation() throws Exception {
        test(UsesXMLDeserialisation.class,Feature.CALLSITE_FOR_java_beans_XMLDecoder__);
    }

    @Test
    public void testUsesJavaCompiler() throws Exception {
        test(UsesJavaCompiler.class,Feature.CALLSITE_FOR_javax_tools_JavaCompiler_);
    }

    @Test
    public void testUsesToolProvider() throws Exception {
        test(UsesJavaCompiler.class,Feature.CALLSITE_FOR_javax_tools_ToolProvider_);
    }

    @Test
    public void testUsesScript() throws Exception {
        test(UsesScript.class,Feature.CALLSITE_FOR_javax_script__);
    }
}
