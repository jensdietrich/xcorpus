package test.nz.ac.massey.cs.xcorpus.stats;

import nz.ac.massey.cs.xcorpus.featureanalysis.AnalysisContext;
import nz.ac.massey.cs.xcorpus.featureanalysis.AnalysisListener;
import nz.ac.massey.cs.xcorpus.featureanalysis.Feature;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Listener for testing purposes, simply stores features in a collection, from where they can be queried by tests.
 * @author jens dietrich
 */
public class TestAnalysisListener implements AnalysisListener {

    Collection<Feature> foundFeatures = new ArrayList<>();

    @Override
    public void featureFound(AnalysisContext context, Feature feature) {
        foundFeatures.add(feature);
    }
}
