package testdata;

import java.util.function.Predicate;

public class UsesFunction {
    public boolean foo(Predicate<String> filter) {
        return filter.test("42");
    }
}
