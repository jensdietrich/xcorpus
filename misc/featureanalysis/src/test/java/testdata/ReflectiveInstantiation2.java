package testdata;

public class ReflectiveInstantiation2 {
    public Object foo() throws Exception {
        return Object.class.newInstance();
    }
}
