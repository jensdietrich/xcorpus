package testdata;

import java.beans.XMLDecoder;
import java.io.FileInputStream;

public class UsesXMLDeserialisation {

    public static Object foo() throws Exception {
        XMLDecoder d = new XMLDecoder(new FileInputStream(""));
        return d.readObject();
    }

}
