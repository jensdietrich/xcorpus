package testdata;

import java.util.List;

public class UsesGenericLocalVariable {
    Object foo() {
        List<String> var = null;
        return var;
    }
}
