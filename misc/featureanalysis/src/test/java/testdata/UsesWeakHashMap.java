package testdata;

import java.util.Map;
import java.util.WeakHashMap;

public class UsesWeakHashMap {
    public void foo() {
        Map map = new WeakHashMap<>();
        System.out.println(map);
    }
}
