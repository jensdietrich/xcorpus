package testdata;

import java.lang.ref.SoftReference;

public class UsesSoftReference {
    public void foo() {
        SoftReference ref = new SoftReference(new Object());
        System.out.println(ref);
    }
}
