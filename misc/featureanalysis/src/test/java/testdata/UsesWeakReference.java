package testdata;

import java.lang.ref.WeakReference;

public class UsesWeakReference {
    public void foo() {
        WeakReference ref = new WeakReference(new Object());
        System.out.println(ref);
    }
}
