package testdata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class UsesAnnotations<@ATypeParameterAnnotation T> {

    @Target(ElementType.PACKAGE)@Retention(RetentionPolicy.RUNTIME) @interface PackageAnnotation{}
    @Target(ElementType.METHOD)@Retention(RetentionPolicy.RUNTIME) @interface MethodAnnotation{}
    @Target(ElementType.FIELD)@Retention(RetentionPolicy.RUNTIME) @interface FieldAnnotation{}
    @Target(ElementType.PARAMETER)@Retention(RetentionPolicy.RUNTIME) @interface ParameterAnnotation{}


    // http://www.eclipse.org/aspectj/doc/released/adk15notebook/annotations.html
    // "Local variable annotations are not retained in class files (or at runtime) regardless of the retention policy set on the annotation type. See JLS 9.6.1.2."
    @Target(ElementType.LOCAL_VARIABLE)@Retention(RetentionPolicy.RUNTIME) @interface LocalVariableAnnotation{}

    @FieldAnnotation
    private String field = null;

    @MethodAnnotation
    public String foo (@ParameterAnnotation String arg) throws @ATypeUseAnnotation Exception {
        @LocalVariableAnnotation Object v = "hi";
        String s = (@ATypeUseAnnotation String)v;
        return "hi";
    }

}
