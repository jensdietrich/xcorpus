package testdata;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.function.Predicate;

public class UsesProxy {

    public static class Handler implements InvocationHandler {
        @Override
        public Object invoke(Object obj, Method m, Object[] arg) throws Throwable {
            System.out.println("Method " + m.getName() + " called");
            return m.invoke(obj,arg);
        }
    }

    public static void foo() {
        Predicate proxy = (Predicate) Proxy.newProxyInstance(Predicate.class.getClassLoader(),new Class[] { Predicate.class },new Handler());
        proxy.test("42");
    }

}
