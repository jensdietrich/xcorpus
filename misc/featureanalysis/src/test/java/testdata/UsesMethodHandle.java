package testdata;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

public class UsesMethodHandle {
    public static void foo() throws Throwable {
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodHandle mh = lookup.findStatic(UsesMethodHandle.class, "foo", MethodType.methodType(void.class));
        mh.invokeExact();
    }
}
