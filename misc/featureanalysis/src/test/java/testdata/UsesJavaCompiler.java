package testdata;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

public class UsesJavaCompiler {
    public Object foo() {

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        return compiler.getTask(null,null,null,null,null,null);
    }
}
