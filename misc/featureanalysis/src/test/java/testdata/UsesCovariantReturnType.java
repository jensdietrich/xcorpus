package testdata;


import java.util.HashMap;
import java.util.List;

public class UsesCovariantReturnType extends HashMap<String,String> {
    // use co-variant return types to generate synthetic bridge method
    @Override
    public List<String> values() {
        return (List<String>) super.values();
    }
}
