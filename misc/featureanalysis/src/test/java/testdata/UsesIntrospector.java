package testdata;


import java.beans.BeanInfo;
import java.beans.Introspector;

public class UsesIntrospector {

    public static void foo () throws Exception {
        Class clazz = Class.forName("java.util.String");
        BeanInfo info = Introspector.getBeanInfo(clazz);
        System.out.println(info);
    }
}
