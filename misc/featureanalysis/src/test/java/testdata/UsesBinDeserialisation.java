package testdata;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class UsesBinDeserialisation {

    public static Object foo() throws Exception {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(""));
        return in.readObject();
    }

}
