package testdata;

import java.util.List;

public class UsesGenericMethod1 {

    public void foo(List<String> arg) {
        System.out.println(arg);
    }
}
