package testdata;

public class ReflectiveInstantiation1 {
    public Object foo() throws Exception {
        java.lang.reflect.Constructor constructor = Object.class.getConstructor(new Class[]{});
        return constructor.newInstance(new Object[]{});
    }
}
