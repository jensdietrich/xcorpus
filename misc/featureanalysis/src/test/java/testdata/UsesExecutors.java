package testdata;


import java.util.concurrent.Executors;

public class UsesExecutors {

    public static void foo () throws Exception {
        Executors.callable(() -> System.out.println("foo")).call();
    }
}
