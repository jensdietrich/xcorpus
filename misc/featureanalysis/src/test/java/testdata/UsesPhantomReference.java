package testdata;

import java.lang.ref.PhantomReference;

public class UsesPhantomReference {
    public void foo() {
        PhantomReference ref = new PhantomReference(new Object(),null);
        System.out.println(ref);
    }
}
