package testdata;


import java.util.stream.Stream;

public class UsesLambda {
    Object[] foo() {
        return Stream.of("aa","ab","b").filter(s -> s.startsWith("a")).toArray();
    }
}
