package testdata;

import sun.misc.Unsafe;

public class UsesUnsafe {
    public Unsafe foo() {
        return Unsafe.getUnsafe();
    }
}
