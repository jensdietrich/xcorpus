##Feature Analysis

This script analyses the use of language and platform features by the programs in the corpus. Feature analysis is based on byte code analysis.

To run feature analysis, run `ant analyse-feature-use` from this directory. Note that this will require network access as ant needs to resolve dependencies via ivy.

Results will be written to `./output` -- a summary file formatted as latex table, and details in csv format.

The script should take less than a min to complete, plus time needed for dependency resultion when the script is executed for the first time. 

##Test Count

This script counts built-in and generated test cases using a combination of source code (AST) analysis (for generated tests) and byte code analysis (for built-in tests).  To start the script, execute `ant count-tests` from this folder.

Results are reported on the console. 
