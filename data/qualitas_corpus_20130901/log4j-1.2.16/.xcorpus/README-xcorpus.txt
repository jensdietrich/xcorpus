log4j-1.2.16

INTRODUCTION
------------
This is a modified version of the log4j-1.2.16 project. 
The executable original is available in the download section of www.qualitascorpus.com

For information concerning the use of the original project see http://logging.apache.org/log4j

MODIFICATIONS
-------------
For purposes of the xcorpus project, several changes have been applied to the original project architecture:

DELETED bin folder       - own binaries folder will be created during build process
ADDED exercise.xml       - ant build file
ADDED yvi.xml            - yvi file for project dependencies
ADDED README-xcorpus.txt - this file
REPLACED libraries       - with ivy dependencies
     EXCEPTIONS          - avro-ipc-1.5.0.jar
                         - logback-core-0.9.27-tests.jar
                         - mockejb.jar
                         - org.springframework.context.jar
                         - org.springframework.web.servlet-3.0.0.M3.jar
                         - servlet-api-2.3.jar
     The libraries that could not be replaced have been moved to the ./default-lib folder.

	 
ANALYSIS
--------
The project can be compiled, executed and analysed via the ant build file.
Important ant targets:
    clean                - Deletes all data that can be regenerated via compiling or execution.
	compile              - Compiles the project.
	compiletests         - Depends on the compile target and additionally compiles the built-in tests.
	builtin-test         - Depends on compiletests. It executes all currently succeeding tests and creates coverage metrics. Test results will be stored in the "testreport" directory, the coverage metrics in the "coveragemetrics" directory.
	builtin-test-failing - Analogue to the builtin-test target but executes all currently failing tests.
